import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
from pathlib import Path
from bz2 import BZ2File as bzopen
from json import loads, dumps
import time
import itertools
#import simplejson
#import Levenshtein
import re
#import ijson
import networkx as nx
import math
from collections import Counter
from operator import itemgetter
from itertools import groupby
import dask.dataframe as dd

rootDirectory = "../archiveteam-twitter-stream-2018-02/2018"
csvDirectory = "../CSV Files"

def read_tweets() :
    
    '''
    This method is used to iterate through all the files included in a Twitter archive directory, in order to read all the tweets
    recorded and parse the data related to the user.
    The fields saved in a csv file are the following :
    
        - the ID and the screen name of the user posting the tweet
        - the ID and the screen name of the user being replied to (if the parsed tweet is an answer to another tweet)
    '''
    
    filenameCount = 0
    csvPath = Path("../../../../../../mnt/ntfs/Amin Mekacher/Twitter Archive/CSV Files/Apr 2018/twitterUsers%d.csv" % filenameCount)
    
    
    if csvPath.is_file():
        newFile = False
    else:
        newFile = True
    
    for dirpath, dirnames, filenames in os.walk(rootDirectory):
        if len(filenames) > 1: # at least one file is nested in the directory
            for file in filenames:
                
                csvPath = Path("../../../../../../mnt/ntfs/Amin Mekacher/Twitter Archive/CSV Files/Apr 2018/twitterUsers%d.csv" % 
                               filenameCount)
                filepath = os.path.join(dirpath, file)

                if file[-3:] == 'bz2' and os.stat(filepath).st_size > 1000000:             
                   
                    with bzopen(filepath, "r") as bztext:
                        tweet_list = list(enumerate(bztext))
                        tweet_dict = [loads(tweet_list[i][1]) for i in range(0, len(tweet_list))]
                        
                        # Create list with users who posted a tweet
                        userIdList = [tweet['user']['id_str'] for tweet in tweet_dict if 'created_at' in tweet]
                        screenNameList = [tweet['user']['screen_name'] for tweet in tweet_dict if 'created_at' in tweet]
                        usersList = [{'id': userIdList[index], 'screen_name': screenNameList[index]} 
                                         for index in range(0, len(userIdList))]
                        
                        # Create list with users in reply
                   
                        replyIdList = [tweet['in_reply_to_user_id_str'] for tweet in tweet_dict 
                                       if 'created_at' in tweet]
                        replyScreenNameList = [tweet['in_reply_to_screen_name'] for tweet in tweet_dict 
                                       if 'created_at' in tweet]
                        replyList = [{'id': replyIdList[index], 'screen_name': replyScreenNameList[index]}
                                            for index in range(0, len(replyIdList)) if replyIdList[index] is not None]
                        
                        # Merge the two lists (twittersList and replyList) in a single list
                        usersList.extend(replyList)                        
                        
                        fileDF = pd.DataFrame(usersList)
                        
                        with open(csvPath, 'a') as userFile:
                            if newFile:
                                newFile = False
                                fileDF.to_csv(userFile, header=True, index=False)
                            else:
                                fileDF.to_csv(userFile, header=False, index=False)
                                
                            fsize = os.stat(csvPath)
                            if fsize.st_size > 100000000:
                                filenameCount += 1
                                
                if os.path.isfile(filepath):
                    os.remove(filepath)
                    fileCount += 1
                    
def id_sorting():
    
    '''
    Method defined to parse all the users saved on the csv files in a single dictionary, to merge the data saved in the different
    files and organize them with a (key, value) paradigm, where the key represents the ID and the value is a list containing
    every screen name associated to that ID.
    '''
    
    # STEP 1: read all files and aggregate their data into a single Dask Dataframe
    
    for digit1 in range(1, 10):
        for digit2 in range(0, 10):
            for dirpath, dirnames, filenames in os.walk(csvDirectory):

                if len(filenames) > 1: # at least one file is nested in the directory
                    for file in filenames:

                        filepath = os.path.join(dirpath, file)

                        tweetDF = dd.read_csv(filepath, header=0, dtype={'id': int})
                            
                        tweetDF.columns = ['id', 'screen_name']
                        tweetDF['firstDigit'] = tweetDF.id.astype(str).str[0].astype(int)
                        tweetDF['secondDigit'] = tweetDF.id.astype(str).str[1].astype(int)
                        tweetDF = tweetDF[tweetDF['firstDigit'] == digit1].drop_duplicates()
                        tweetDF = tweetDF[tweetDF['secondDigit'] == digit2].drop_duplicates()
                        
                        tweetDF = tweetDF.drop(['firstDigit', 'secondDigit'], axis=1)
                        tweetDF = tweetDF.compute()

                        digitPath = Path("Digit Files/twitter" + str(digit1) + str(digit2) + ".csv")
                        
                        with open(digitPath, 'a') as userFile:
                            if digitPath.is_file():
                                tweetDF.to_csv(userFile, header=False, index=False)
                            else:
                                tweetDF.to_csv(userFile, header=True, index=False)
    
def detect_samsa_accounts():
    
    '''
    This method is called after sorting all the Tweeter accounts by their ID, in order to detect if a specific ID is associated
    with more than one screen name. This list is then written in a new CSV document, to be used afterwards to get their
    specific tweets.
    '''
    
    digitDirectory = "Digit Files"
    tweetDict = {}
    samsaList = []
    count = 0
    
    for dirpath, dirnames, filenames in os.walk(digitDirectory):

        if len(filenames) >= 1: # at least one file is nested in the directory
            for file in filenames:
                
                print("File: ", file)
                
                timeSt = time.time()
                timeCount = time.time()
                
                filepath = os.path.join(dirpath, file)
                csvDF = pd.read_csv(filepath, names=['id', 'screen_name']).drop_duplicates()
                print("File read: ", len(csvDF))
                for index, user in csvDF.iterrows():
                    count += 1
                    if np.mod(count, 50000) == 0:
                        print("Count: ", count, time.time() - timeCount)
                        timeCount = time.time()
                        print("List: ", samsaList[-50:], len(samsaList))
                    if user['id'] not in tweetDict:
                        tweetDict[user['id']] = [user['screen_name']]
                    else:
                        if user['id'] not in samsaList:
                            samsaList.append(user['id'])
                                   
                with open("samsa.txt", "a") as samsaFile:
                    simplejson.dump(samsaList, samsaFile)
                    samsaList = []
    
                    samsaFile.close()
        
def clean_samsa_text():
    '''
    Calling this method cleans the .txt file returned by the detect_samsa_accounts method, by aggregating every entry into a single 
    list
    '''
    
    idFile = open("samsa.txt", "r")
    lines = idFile.read()
    charList = []
    
    samsaUnfiltered = ""
    for char in lines:
        if char == ']':
            samsaUnfiltered += ','
        elif char == '[':
            samsaUnfiltered += ''
        else:
            samsaUnfiltered += char
            
        if char not in charList:
            charList.append(char)
    
    with open("samsaUnfiltered.txt", "a") as samsaFile:
        samsaFile.write(samsaUnfiltered)
        
    return charList
        
def filter_samsa_accounts(idList, filtering):
    
    '''
    Method used to filter the flagged Samsa accounts by using the similarity between their associated screen names. If the 
    maximum Levenshtein distance between the screen names is above a specific threshold, then the account is considered. It
    is discarded otherwise. Before computing the edit distance, the digits at the end of the screen names are removed 
    (for instance jahl323 -> jahl) in order not to take into account the users who just changed those digits
    '''
    
    filteredIdList = []
    screenNameList = [] # list used to store the number of screen names associated with each account
    levenshteinList = [] # list used to store the maximum levenshtein distance between screen names of a single ID
    samsaListFiltered = []
    
    digitTime = time.time()
    
    for firstDigit in range(1, 10):
        firstTime = time.time()
        for secondDigit in range(0, 10):
            print("Digit start: ", firstDigit, secondDigit)
            timeDigit = time.time()
            idDigitList = [idFilt for idFilt in idList if int(idFilt[0]) == firstDigit and int(idFilt[1]) == secondDigit]
            
            digitFile = "Digit Files/twitter" + str(firstDigit) + str(secondDigit) + ".csv"
            
            if Path(digitFile).is_file():
                digitDF = pd.read_csv(digitFile, names=['id', 'screen_name']).drop_duplicates() 

                for idDigit in idDigitList:
                    idDigitDF = digitDF.loc[digitDF['id'] == int(idDigit)]
                    idNameList = idDigitDF.screen_name.unique() 
                    idNameListFiltered = []

                    if filtering:
                        for idName in idNameList:
                            idNameFiltered = ''.join(filter(str.isalpha, idName)).lower()
                            idNameListFiltered.append(idNameFiltered)

                    else:
                        idNameListFiltered = idNameList
                        

                    # Compute the Levenshtein distances between each screen name and keep only the maximum one
                    levenshteinDistance = np.max([Levenshtein.distance(screenName1, screenName2) 
                                           for screenName1, screenName2 in itertools.permutations(idNameListFiltered, r=2)])

                    levenshteinList.append(levenshteinDistance)
                    
                    idNameListDuplicate = list(set(idNameListFiltered)) # Create a new list with the duplicates removed
                    screenNameList.append(len(idNameListDuplicate)) ## TODO: remove duplicates
                    
                    # If the number of different screen names is still bigger than 1, we add this user in the Samsa list
                    if len(idNameListDuplicate) > 1:
                        samsaListFiltered.append(idDigit)
                    
                
        print("First digit done: ", time.time() - firstTime)
                                   
            
    print("Work done: ", time.time() - digitTime)
    
    # FIRST PLOT: histogram of the edit distance
    
    num_bins = np.max(levenshteinList)
    n, bins, patches = plt.hist(levenshteinList, num_bins, facecolor='blue', alpha=0.5)
    plt.xlabel('Max edit distance')
    plt.ylabel('# of accounts')
    plt.title('Maximum edit distance between screen names of a same account')
    plt.show()
    plt.savefig('editDistanceFiltered.png')
    
    
    # SECOND PLOT: histogram of the number of screen names
    
    screenNameListCeil = [screenName for screenName in screenNameList if screenName <= 15]
    num_bins = np.max(screenNameListCeil)
    n, bins, patches = plt.hist(screenNameListCeil, num_bins, facecolor='blue', alpha=0.5)
    plt.xlabel('Number of screen names')
    plt.ylabel('# of accounts')
    plt.title('Histogram of the number of screen names associated with each account')
    plt.show()
    plt.savefig('screenNameFiltered.png')
    
    
    return samsaListFiltered
        

def retrieve_samsa_tweets(samsaListFiltered, folderPath, lastDayFolder):
    
    '''
    Goal of the function: retrieve any tweet related to the Samsa accounts in a separate archive. This includes any tweet posted 
    by these accounts, any reply to one of their tweets or any retweet. The tweets are then saved in files on a daily basis
    '''
    
    tweetSamsaList = []
    setTime = time.time()
    samsaSetFiltered = set(samsaListFiltered)

    for dirpath, dirnames, filenames in os.walk(folderPath):
        print("Dir: ", dirpath)
        dirList = dirpath.split(os.sep)
        
        # When we reach the end of a day, dump the tweetSamsaList into a Json file
        if len(dirList) == 5 and dirList[-1:][0] != lastDayFolder:
            lastDayFolder = dirList[-1:][0]
                
        if len(filenames) > 1: # at least one file is nested in the directory
            for file in filenames:
                
                filepath = os.path.join(dirpath, file)

                # List of tweets related to any Samsa account

                if file[-3:] == 'bz2' and os.stat(filepath).st_size > 1000000:             
                    
                    with bzopen(filepath, "r") as bztext:
                        fileTime = time.time()
                        tweet_list = list(enumerate(bztext))
                        tweet_dict = [loads(tweet_list[i][1]) for i in range(0, len(tweet_list)) if 'created_at' in loads(tweet_list[i][1])]
            
                        tweetSamsaList = [tweet for tweet in tweet_dict if tweet['user']['id_str'] in samsaSetFiltered or 
                                                                           tweet['in_reply_to_user_id_str'] in samsaSetFiltered]
                        with open(folderPath + "samsa" + lastDayFolder + ".json", "a") as samsaArchiveFile:
                            simplejson.dump(tweetSamsaList, samsaArchiveFile)
                            samsaArchiveFile.close()
                            
                        if os.path.isfile(filepath):
                            os.remove(filepath)
                            
                            
def clean_json_files(filePath):
    
    '''
    Method used to clean the Json files written by the retrieve_samsa_function, in order to correct any parsing error happening.
    This function concatenates every list present in a Json file into a single list.
    '''

    with open(filePath) as samsaFile:
        samsaText = samsaFile.read()

        samsaTextCleaned = samsaText.replace('][', ',')
        with open(filePath, 'w') as writeFile:
            writeFile.write(samsaTextCleaned)
            
            
def language_filtering(dirpath, file, folder):
    
    '''
    The next filtering step consists of keeping only the tweets posted by accounts whose languages is among the ones we want
    to study (i.e lang in ['en', 'es', 'fr', 'ru', 'uk', 'tr', 'de']). This method will be used to create a new database, by 
    keeping only the tweets posted by users with one of these languages in their profile
    '''
    filePath = os.path.join(dirpath, file)
    langFiltered = ['en', 'es', 'fr', 'ru', 'uk', 'tr', 'de']
    totalCount, langCount = 0, 0
    langTime = time.time()
    
    langList = []
    
    with open(filePath, 'rb') as jsonFile:
        objects = ijson.items(jsonFile, 'item')
        tweets = (o for o in objects)
        for tweet in tweets:
            totalCount += 1
            if tweet['lang'] in langFiltered:
                langCount += 1
                langList.append(tweet)
                
        
        print("Counts: ", langCount, totalCount)
    
    languageFile = os.path.join("Lang", folder, file[:-5] + "_lang"  + ".json")
    print("language: ", languageFile)
    
    with open(languageFile, 'w') as jsonFilteredFile:
        simplejson.dump(langList, jsonFilteredFile)
        jsonFilteredFile.close()
        
    print("Total time: ", time.time() - langTime)
    
def language_shift_detection():
    
    '''
    Method used to study if any Samsa candidate has used different languages throughout the year 2018, by building a dataframe
    listing every Samsa candidate and the number of tweets they posted in each language
    '''
    
    langDir = 'Lang/'
    dfColumns = ['samsa_id', 'en', 'es', 'fr', 'ru', 'uk', 'tr', 'de']
    
    langDf = pd.DataFrame(columns=dfColumns)
    tweetCount = 0
    
    for dirpath, dirnames, filenames in os.walk(langDir):
        if len(filenames) >= 1: # at least one file is nested in the directory
            for file in filenames:
                
                filePath = os.path.join(dirpath, file)
                with open(filePath, 'rb') as jsonFile:
                    objects = ijson.items(jsonFile, 'item')
                    tweets = (o for o in objects)
                    print("Starting file: ", filePath)
                    
                    langDict = [{'id': tweet['user']['id_str'], 'lang': tweet['lang']} for tweet in tweets]
                    idListSorted = sorted(list(set([langTweet['id'] for langTweet in langDict])))
                    
                    langDictGrouped = groupby(sorted(langDict, key=itemgetter('id')) , key=itemgetter('id'))

                    langGroup = [[tweet['lang'] for tweet in list(groupList)] for idUser, groupList in langDictGrouped]
                    
                    langCount = [Counter(group) for group in langGroup]
                    
                    langIdDict = [dict(countDict, **{'samsa_id':idUser}) for countDict, idUser in zip(langCount, idListSorted)]
                    
                    langDataframe = pd.DataFrame(columns=dfColumns)
                    langDataframe = langDataframe.append(langIdDict)
                    langDataframe = langDataframe.fillna(0)
                    langDataframe[dfColumns] = langDataframe[dfColumns].applymap(np.int32)
                    
                    with open('samsaLang.csv', 'a') as samsaLangFile:
                        langDataframe.to_csv(samsaLangFile, sep='\t')   
                        
def merge_dataframe():
    
    '''
    This method is used to open the dataframe created with the language_shift_detection function, in order to sum up every row relative to the same ID. The new
    dataframe is then saved in another .csv file
    '''
    
    '''
    dfColumns = ['samsa_id', 'en', 'es', 'fr', 'ru', 'uk', 'tr', 'de']
    langDataframe = pd.read_csv('samsaLang.csv', sep='\t',  usecols=dfColumns)
    print("columns: ", langDataframe.columns)
    idList = langDataframe.samsa_id.unique()
    
    print("ID List: ", idList[0:20], len(idList))
    print("Lang before: ", langDataframe, langDataframe.shape)
    
    langDataframe = langDataframe.groupby(['samsa_id'], as_index=False).sum()
    print("Lang after: ", langDataframe, langDataframe.shape)
    
    with open('samsaLangMerged.csv', 'a') as samsaLangFile:
        langDataframe.to_csv(samsaLangFile, sep='\t') 
    print(ok)
    '''
    dfColumns = ['samsa_id', 'en', 'es', 'fr', 'ru', 'uk', 'tr', 'de']
    langDask = dd.read_csv('samsaLang.csv', sep='\t', usecols=dfColumns, dtype={'de': 'object',
       'en': 'object',
       'es': 'object',
       'fr': 'object',
       'ru': 'object',
       'samsa_id': 'object',
       'tr': 'object',
       'uk': 'object'})
    
    print("Read file")
    langDask = langDask.groupby('samsa_id').sum()
    print("Groupby ok")
    
    daskTest = langDask.head().compute()
    print("Test: ", daskTest)
    daskTest.to_csv('langDask.csv')
                        
def samsa_network_creation(samsaNetwork):
    
    '''
    Goal: create a network containing two types of nodes
    
        - A node for each of the Samsa user
        - Some nodes representing each of the selected language (i.e Russian, French, English, Turkish, Spanish, Ukrainian, German)
        
    Every time a tweet is processed, a new Samsa node is created if it does not already appear, and an edge between this user and the language in which they tweeted
    is added to the network. Each Samsa node will also have a property related to their profile language, which is set to "shifted" if more than one language is
    detected in the archive
    '''
    
    langDir = 'Lang/Apr 2018'
    
    # Add the language nodes to the network
    
    langNodes = ['en', 'es', 'fr', 'ru', 'uk', 'tr', 'de']
    samsaNetwork.add_nodes_from(langNodes, profileLanguage = 'None')
    
    for dirpath, dirnames, filenames in os.walk(langDir):
        if len(filenames) >= 1:
            for file in filenames:
                
                filePath = os.path.join(dirpath, file)
                print("Starting file: ", filePath)
                
                with open(filePath, 'rb') as jsonFile:
                    objects = ijson.items(jsonFile, 'item')
                    tweets = (o for o in objects)
                    for tweet in tweets:
                        
                        # Process the data relative to the user
                        
                        idUser = tweet['user']['id_str']
                        langUser = tweet['user']['lang']
                        followerUser = tweet['user']['followers_count']
                        if idUser not in samsaNetwork.nodes():
                            samsaNetwork.add_node(idUser)
                            samsaNetwork.nodes[idUser]['profileLanguage'] = langUser
                            samsaNetwork.nodes[idUser]['followersCount'] = followerUser
                            #print("New user: ", samsaNetwork.nodes[idUser]['profileLanguage'], samsaNetwork.nodes[idUser]['followersCount'])
                        else:
                            if langUser is not samsaNetwork.nodes[idUser]['profileLanguage']:
                                samsaNetwork.nodes[idUser]['profileLanguage'] = 'shifted'
                                
                            if followerUser > samsaNetwork.nodes[idUser]['followersCount']:
                                samsaNetwork.nodes[idUser]['followersCount'] = followerUser
                                
                        # Process the data relative to the tweet
                        
                        langTweet = tweet['lang']
                        
                        if langTweet not in samsaNetwork.nodes():
                            langNodes.append(langTweet)
                            samsaNetwork.add_node(langTweet, profileLanguage='None')
                            print("New language: ", langTweet)
                        
                        if samsaNetwork.has_edge(idUser, langTweet):
                            samsaNetwork[idUser][langTweet]['weight'] += 1
                        else:
                            samsaNetwork.add_edge(idUser, langTweet, weight = 1)
                            
                            
            # Split the graph in multiple graphs, relative to the nodes' profileLanguage attribute
            
            samsaGerman = samsaNetwork.copy()
            samsaGerman.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'de' and node not in langNodes])
            nx.write_gexf(samsaGerman, 'samsaGermanapr.gexf', version="1.2draft")
            
            samsaEnglish = samsaNetwork.copy()
            samsaEnglish.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'en' and node not in langNodes])
            nx.write_gexf(samsaEnglish, 'samsaEnglishapr.gexf', version="1.2draft")
            
            samsaFrench = samsaNetwork.copy()
            samsaFrench.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'fr' and node not in langNodes])
            nx.write_gexf(samsaFrench, 'samsaFrenchapr.gexf', version="1.2draft")
            
            samsaRussian = samsaNetwork.copy()
            samsaRussian.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'ru' and node not in langNodes])
            nx.write_gexf(samsaRussian, 'samsaRussianapr.gexf', version="1.2draft")
            
            samsaUkrainian = samsaNetwork.copy()
            samsaUkrainian.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'uk' and node not in langNodes])
            nx.write_gexf(samsaUkrainian, 'samsaUkrainianapr.gexf', version="1.2draft")
            
            samsaTurkish = samsaNetwork.copy()
            samsaTurkish.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'tr' and node not in langNodes])
            nx.write_gexf(samsaTurkish, 'samsaTurkishapr.gexf', version="1.2draft")
            
            samsaSpanish = samsaNetwork.copy()
            samsaSpanish.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'es' and node not in langNodes])
            nx.write_gexf(samsaSpanish, 'samsaSpanishapr.gexf', version="1.2draft")
            
            samsaShifted = samsaNetwork.copy()
            samsaShifted.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'shifted' and node not in langNodes])
            nx.write_gexf(samsaShifted, 'samsaShiftedapr.gexf', version="1.2draft")
            
            print("Before: ", len(samsaNetwork.nodes()))
            nodesToRemove = []
            for (node, degree) in samsaNetwork.degree:
                if degree == 1:
                    nodesToRemove.append(node)
                    
            samsaNetwork.remove_nodes_from(nodesToRemove)
            nx.write_gexf(samsaNetwork, 'apr2018.gexf', version="1.2draft")

            
            print("--TOTAL NUMBER OF NODES--")
            print("German: ", len(samsaGerman.nodes()), '\n',
                  "French: ", len(samsaFrench.nodes()), '\n',
                  "Spanish: ", len(samsaSpanish.nodes()), '\n',
                  "Russian: ", len(samsaRussian.nodes()), '\n',
                  "Ukrainian: ", len(samsaUkrainian.nodes()), '\n',
                  "Turkish: ", len(samsaTurkish.nodes()), '\n',
                  "English: ", len(samsaEnglish.nodes()), '\n',
                  "Shifted: ", len(samsaShifted.nodes()), '\n',
                  "Total: ", len(samsaNetwork.nodes()), '\n')
                        
                        
                        
    
            
                            
                            
                           
                            
                         
                            
                        
                        
                    
                
            
    
    
    
    
                                  
                                
                