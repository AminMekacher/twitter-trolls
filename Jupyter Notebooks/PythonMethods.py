import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
from pathlib import Path
from bz2 import BZ2File as bzopen
from json import loads, dumps
import time
import itertools
import simplejson
import Levenshtein
import re
import ijson
import networkx as nx
import math
from collections import Counter, defaultdict
from operator import itemgetter
from itertools import groupby
import multiprocessing
import random
from datetime import datetime
#import dask.dataframe as dd

rootDirectory = "../archiveteam-twitter-stream-2018-02/2018"
csvDirectory = "../CSV Files"

def read_tweets() :
    
    '''
    This method is used to iterate through all the files included in a Twitter archive directory, in order to read all the tweets
    recorded and parse the data related to the user.
    The fields saved in a csv file are the following :
    
        - the ID and the screen name of the user posting the tweet
        - the ID and the screen name of the user being replied to (if the parsed tweet is an answer to another tweet)
    '''
    
    filenameCount = 0
    csvPath = Path("../../../../../../mnt/ntfs/Amin Mekacher/Twitter Archive/CSV Files/Apr 2018/twitterUsers%d.csv" % filenameCount)
    
    
    if csvPath.is_file():
        newFile = False
    else:
        newFile = True
    
    for dirpath, dirnames, filenames in os.walk(rootDirectory):
        if len(filenames) > 1: # at least one file is nested in the directory
            for file in filenames:
                
                csvPath = Path("../../../../../../mnt/ntfs/Amin Mekacher/Twitter Archive/CSV Files/Apr 2018/twitterUsers%d.csv" % 
                               filenameCount)
                filepath = os.path.join(dirpath, file)

                if file[-3:] == 'bz2' and os.stat(filepath).st_size > 1000000:             
                   
                    with bzopen(filepath, "r") as bztext:
                        tweet_list = list(enumerate(bztext))
                        tweet_dict = [loads(tweet_list[i][1]) for i in range(0, len(tweet_list))]
                        
                        # Create list with users who posted a tweet
                        userIdList = [tweet['user']['id_str'] for tweet in tweet_dict if 'created_at' in tweet]
                        screenNameList = [tweet['user']['screen_name'] for tweet in tweet_dict if 'created_at' in tweet]
                        usersList = [{'id': userIdList[index], 'screen_name': screenNameList[index]} 
                                         for index in range(0, len(userIdList))]
                        
                        # Create list with users in reply
                   
                        replyIdList = [tweet['in_reply_to_user_id_str'] for tweet in tweet_dict 
                                       if 'created_at' in tweet]
                        replyScreenNameList = [tweet['in_reply_to_screen_name'] for tweet in tweet_dict 
                                       if 'created_at' in tweet]
                        replyList = [{'id': replyIdList[index], 'screen_name': replyScreenNameList[index]}
                                            for index in range(0, len(replyIdList)) if replyIdList[index] is not None]
                        
                        # Merge the two lists (twittersList and replyList) in a single list
                        usersList.extend(replyList)                        
                        
                        fileDF = pd.DataFrame(usersList)
                        
                        with open(csvPath, 'a') as userFile:
                            if newFile:
                                newFile = False
                                fileDF.to_csv(userFile, header=True, index=False)
                            else:
                                fileDF.to_csv(userFile, header=False, index=False)
                                
                            fsize = os.stat(csvPath)
                            if fsize.st_size > 100000000:
                                filenameCount += 1
                                
                if os.path.isfile(filepath):
                    os.remove(filepath)
                    fileCount += 1
                    
def id_sorting():
    
    '''
    Method defined to parse all the users saved on the csv files in a single dictionary, to merge the data saved in the different
    files and organize them with a (key, value) paradigm, where the key represents the ID and the value is a list containing
    every screen name associated to that ID.
    '''
    
    # STEP 1: read all files and aggregate their data into a single Dask Dataframe
    
    for digit1 in range(1, 10):
        for digit2 in range(0, 10):
            for dirpath, dirnames, filenames in os.walk(csvDirectory):

                if len(filenames) > 1: # at least one file is nested in the directory
                    for file in filenames:

                        filepath = os.path.join(dirpath, file)

                        tweetDF = dd.read_csv(filepath, header=0, dtype={'id': int})
                            
                        tweetDF.columns = ['id', 'screen_name']
                        tweetDF['firstDigit'] = tweetDF.id.astype(str).str[0].astype(int)
                        tweetDF['secondDigit'] = tweetDF.id.astype(str).str[1].astype(int)
                        tweetDF = tweetDF[tweetDF['firstDigit'] == digit1].drop_duplicates()
                        tweetDF = tweetDF[tweetDF['secondDigit'] == digit2].drop_duplicates()
                        
                        tweetDF = tweetDF.drop(['firstDigit', 'secondDigit'], axis=1)
                        tweetDF = tweetDF.compute()

                        digitPath = Path("Digit Files/twitter" + str(digit1) + str(digit2) + ".csv")
                        
                        with open(digitPath, 'a') as userFile:
                            if digitPath.is_file():
                                tweetDF.to_csv(userFile, header=False, index=False)
                            else:
                                tweetDF.to_csv(userFile, header=True, index=False)
    
def detect_samsa_accounts():
    
    '''
    This method is called after sorting all the Tweeter accounts by their ID, in order to detect if a specific ID is associated
    with more than one screen name. This list is then written in a new CSV document, to be used afterwards to get their
    specific tweets.
    '''
    
    digitDirectory = "Digit Files"
    tweetDict = {}
    samsaList = []
    count = 0
    
    for dirpath, dirnames, filenames in os.walk(digitDirectory):

        if len(filenames) >= 1: # at least one file is nested in the directory
            for file in filenames:
                
                print("File: ", file)
                
                timeSt = time.time()
                timeCount = time.time()
                
                filepath = os.path.join(dirpath, file)
                csvDF = pd.read_csv(filepath, names=['id', 'screen_name']).drop_duplicates()
                print("File read: ", len(csvDF))
                for index, user in csvDF.iterrows():
                    count += 1
                    if np.mod(count, 50000) == 0:
                        print("Count: ", count, time.time() - timeCount)
                        timeCount = time.time()
                        print("List: ", samsaList[-50:], len(samsaList))
                    if user['id'] not in tweetDict:
                        tweetDict[user['id']] = [user['screen_name']]
                    else:
                        if user['id'] not in samsaList:
                            samsaList.append(user['id'])
                                   
                with open("samsa.txt", "a") as samsaFile:
                    simplejson.dump(samsaList, samsaFile)
                    samsaList = []
    
                    samsaFile.close()
        
def clean_samsa_text():
    '''
    Calling this method cleans the .txt file returned by the detect_samsa_accounts method, by aggregating every entry into a single 
    list
    '''
    
    idFile = open("samsa.txt", "r")
    lines = idFile.read()
    charList = []
    
    samsaUnfiltered = ""
    for char in lines:
        if char == ']':
            samsaUnfiltered += ','
        elif char == '[':
            samsaUnfiltered += ''
        else:
            samsaUnfiltered += char
            
        if char not in charList:
            charList.append(char)
    
    with open("samsaUnfiltered.txt", "a") as samsaFile:
        samsaFile.write(samsaUnfiltered)
        
    return charList
        
def filter_samsa_accounts(idList, filtering):
    
    '''
    Method used to filter the flagged Samsa accounts by using the similarity between their associated screen names. If the 
    maximum Levenshtein distance between the screen names is above a specific threshold, then the account is considered. It
    is discarded otherwise. Before computing the edit distance, the digits at the end of the screen names are removed 
    (for instance jahl323 -> jahl) in order not to take into account the users who just changed those digits
    '''
    
    filteredIdList = []
    screenNameList = [] # list used to store the number of screen names associated with each account
    levenshteinList = [] # list used to store the maximum levenshtein distance between screen names of a single ID
    samsaListFiltered = []
    
    digitTime = time.time()
    
    for firstDigit in range(1, 10):
        firstTime = time.time()
        for secondDigit in range(0, 10):
            print("Digit start: ", firstDigit, secondDigit)
            timeDigit = time.time()
            idDigitList = [idFilt for idFilt in idList if int(idFilt[0]) == firstDigit and int(idFilt[1]) == secondDigit]
            
            digitFile = "Digit Files/twitter" + str(firstDigit) + str(secondDigit) + ".csv"
            
            if Path(digitFile).is_file():
                digitDF = pd.read_csv(digitFile, names=['id', 'screen_name']).drop_duplicates() 

                for idDigit in idDigitList:
                    idDigitDF = digitDF.loc[digitDF['id'] == int(idDigit)]
                    idNameList = idDigitDF.screen_name.unique() 
                    idNameListFiltered = []

                    if filtering:
                        for idName in idNameList:
                            idNameFiltered = ''.join(filter(str.isalpha, idName)).lower()
                            idNameListFiltered.append(idNameFiltered)

                    else:
                        idNameListFiltered = idNameList
                        

                    # Compute the Levenshtein distances between each screen name and keep only the maximum one
                    levenshteinDistance = np.max([Levenshtein.distance(screenName1, screenName2) 
                                           for screenName1, screenName2 in itertools.permutations(idNameListFiltered, r=2)])

                    levenshteinList.append(levenshteinDistance)
                    
                    idNameListDuplicate = list(set(idNameListFiltered)) # Create a new list with the duplicates removed
                    screenNameList.append(len(idNameListDuplicate))
                    
                    # If the number of different screen names is still bigger than 1, we add this user in the Samsa list
                    if len(idNameListDuplicate) > 1:
                        samsaListFiltered.append(idDigit)
                    
                
        print("First digit done: ", time.time() - firstTime)
                                   
            
    print("Work done: ", time.time() - digitTime)
    
    # FIRST PLOT: histogram of the edit distance
    
    num_bins = np.max(levenshteinList)
    n, bins, patches = plt.hist(levenshteinList, num_bins, facecolor='blue', alpha=0.5)
    plt.xlabel('Max edit distance')
    plt.ylabel('# of accounts')
    plt.title('Maximum edit distance between screen names of a same account')
    plt.show()
    plt.savefig('editDistanceFiltered.png')
    
    
    # SECOND PLOT: histogram of the number of screen names
    
    screenNameListCeil = [screenName for screenName in screenNameList if screenName <= 15]
    num_bins = np.max(screenNameListCeil)
    n, bins, patches = plt.hist(screenNameListCeil, num_bins, facecolor='blue', alpha=0.5)
    plt.xlabel('Number of screen names')
    plt.ylabel('# of accounts')
    plt.title('Histogram of the number of screen names associated with each account')
    plt.show()
    plt.savefig('screenNameFiltered.png')
    
    
    return samsaListFiltered
        
def filter_samsa_candidates(samsaListFiltered):

    '''
    In order to reduce the number of accounts to consider, this method is used to keep only the Samsa candidates
    infered with the filter_samsa_accounts, by using new filtering parameters:
    
        - the maximum edit distance between the screen names must be higher than a specific threshold T_screen
        - the maximum edit distance for the description must be higher than a specific threshold T_desc
        - the profile image must have been changed at least once 
        - the background banner must have been changed at least once
            
    '''
    
    samsaList = [] # empty list, created to store key / value pairs for each Samsa account
    for dirpath, dirnames, filenames in os.walk('Months Folder'):
        dirList = dirpath.split(os.sep)
        if len(filenames) > 1: # at least one file is nested in the directory
            if len(samsaList) > 0:

                samsaDF = pd.DataFrame(samsaList).drop_duplicates()
                samsaFile = os.path.join('samsaCandidates', dirList[-1] + '.csv')
                samsaDF.to_csv(samsaFile)
                samsaList = []
            
            for file in filenames:
                
                filePath = os.path.join(dirpath, file)
                print("File: ", filePath)
                with open(filePath, 'rb') as jsonFile:
                    
                    objects = ijson.items(jsonFile, 'item')
                    tweets = (o for o in objects)
                    startTime = time.time()
                    samsaListDay = [{'id': tweetSamsa['user']['id_str'], 
                                     'profile_image': tweetSamsa['user']['profile_image_url'],
                                     'background_image': tweetSamsa['user']['profile_background_image_url']} 
                                     for tweetSamsa in tweets]
                            
                    samsaList.extend(samsaListDay)
                    samsaList = [dict(t) for t in {tuple(samsaTweet.items()) for samsaTweet in samsaList}]
                    print("Time: ", time.time() - startTime)
                    print("Len list: ", len(samsaList))
                    
def sort_samsa_candidates():
    
    '''
    Method used to sort the Samsa users saved in .csv files by the filter_samsa_candidates method. The goal is to group in a single file any
    entry starting with the same digit, in order to be able to find the rows referring to the same user
    '''
    
    for dirpath, dirnames, filenames in os.walk('samsaCandidates'):
        for file in filenames:
            filePath = os.path.join(dirpath, file)
            monthDF = pd.read_csv(filePath)
            monthDF['id'] = monthDF['id'].astype(str)
            monthDF['firstDigit'] = monthDF.id.str[0].astype(int)
            
            for digit in range(1, 10):
                digitDF = monthDF[monthDF.firstDigit == digit]
                csvPath = os.path.join('samsaDigits', str(digit) + '.csv')
                with open(csvPath, 'a') as csvFile:
                    digitDF.to_csv(csvFile)
                    
def samsa_list_extension(args):
    
    digitDF, idSamsa = args
    idSamsaList = digitDF[digitDF['id'] == idSamsa]
    return idSamsaList
    
def identify_image_modification(filePath):
    
    '''
    By using the csv files generated by the previous method, we can identify the Samsa candidates who changed their profile or background
    image during the year 2018, and filter out the other ones. Those who do not have a background or profile image will also be kept
    '''
    p = multiprocessing.Pool(multiprocessing.cpu_count())
    digitDF = pd.read_csv(filePath, usecols=['id', 'profile_image', 'background_image']).drop_duplicates()
    idList = digitDF.id.unique()
    idSamsaList = []
    
    for i in range(0, len(idList), 10000):
        startMap = time.time()
        idSamsaChunk = idList[i : i + 10000]
        samsaArg = [(digitDF, idSamsa) for idSamsa in idSamsaChunk]
        samsaChunkList = p.map(samsa_list_extension, samsaArg)
        samsaChunkDataframe = pd.concat([samsaDF for samsaDF in samsaChunkList])
        idSamsaList.extend(samsaChunkList)
        digitDF = pd.concat([digitDF, samsaChunkDataframe, samsaChunkDataframe]).drop_duplicates(keep=False)
        
    return idSamsaList

def split_csv_file(csvFile, firstDigit):
    
    '''
    In the cases where the CSV file takes a lot of memory and is therefore slow to process, this method is called
    to split the CSV file by second digit
    '''
    
    digitDF = pd.read_csv(csvFile, usecols=['id', 'profile_image', 'background_image'], header=0).drop_duplicates()
    digitDF['secondDigit'] = digitDF.id.astype(str).str[1]
    
    print("DF: ", len(digitDF))
    
    
    for secondDigit in range(0, 10):
        secondDigitDF = digitDF[digitDF['secondDigit'] == str(secondDigit)].drop_duplicates()
        secondDigitDF = secondDigitDF.drop(['secondDigit'], axis=1)
        filePath = 'samsaDigits/' + str(firstDigit) + "/" + str(secondDigit) + ".csv"
        
        secondDigitDF.to_csv(filePath)

def samsa_candidates_statistics():
    
    '''
    By using the Samsa candidates selected with the identify_image_modification function, it is possible to draw some 
    statistical data regarding these accounts, such as their maximum edit distance for their screen name or the number of
    different screen names they used during the year 2018
    '''
    
    levenshteinList = []
    screenNameList = []
    
    fileList = ['1.txt', '2.txt', '3.txt', '4.txt', '5.txt', '6.txt', '7.txt', '8.txt', '9.txt']
    pathList = ['samsaDigits/' + fileName for fileName in fileList]
   
    for firstDigit in range(1, 10):
        for secondDigit in range(0, 10):
            print("Digits: ", firstDigit, secondDigit)
                
            digitFile = "Digit Files/twitter" + str(firstDigit) + str(secondDigit) + ".csv"
            
            if Path(digitFile).is_file():
                digitDF = pd.read_csv(digitFile, names=['id', 'screen_name']).drop_duplicates() 
                
                filePath = pathList[firstDigit - 1]
                with open(filePath, 'r') as samsaFile:
                    samsaList = [samsaID[1:-1] for samsaID in samsaFile]
                    idDigitList = [samsaID for samsaID in samsaList if int(samsaID[0]) == firstDigit and int(samsaID[1]) == secondDigit] 

                    for idDigit in idDigitList:
                        idDigitDF = digitDF.loc[digitDF['id'] == int(idDigit)]
                        idNameList = idDigitDF.screen_name.unique() 
                        
                        idNameListFiltered = []

                        for idName in idNameList:
                            idNameFiltered = ''.join(filter(str.isalpha, idName)).lower()
                            idNameListFiltered.append(idNameFiltered)

                        if len(idNameListFiltered) > 1:
                            levenshteinDistance = np.max([Levenshtein.distance(screenName1, screenName2) 
                                                for screenName1, screenName2 in itertools.permutations(idNameListFiltered, r=2)])

                            levenshteinList.append(levenshteinDistance)

                        if len(idNameListFiltered) > 0:
                            idNameListDuplicate = list(set(idNameListFiltered)) # Create a new list with the duplicates removed
                            screenNameList.append(len(idNameListDuplicate))
                        
                        
    return levenshteinList, screenNameList
            
            
def retrieve_samsa_tweets(samsaListFiltered, folderPath, lastDayFolder):
    
    '''
    Goal of the function: retrieve any tweet related to the Samsa accounts in a separate archive. This includes any tweet posted 
    by these accounts, any reply to one of their tweets or any retweet. The tweets are then saved in files on a daily basis
    '''
    
    tweetSamsaList = []
    setTime = time.time()
    samsaSetFiltered = set(samsaListFiltered)

    for dirpath, dirnames, filenames in os.walk(folderPath):
        dirList = dirpath.split(os.sep)
        
        # When we reach the end of a day, dump the tweetSamsaList into a Json file
        if len(dirList) == 5 and dirList[-1:][0] != lastDayFolder:
            lastDayFolder = dirList[-1:][0]
                
        if len(filenames) > 1: # at least one file is nested in the directory
            for file in filenames:
                
                filepath = os.path.join(dirpath, file)

                # List of tweets related to any Samsa account

                if file[-3:] == 'bz2' and os.stat(filepath).st_size > 1000000:             
                    
                    with bzopen(filepath, "r") as bztext:
                        fileTime = time.time()
                        tweet_list = list(enumerate(bztext))
                        tweet_dict = [loads(tweet_list[i][1]) for i in range(0, len(tweet_list)) if 'created_at' in loads(tweet_list[i][1])]
            
                        tweetSamsaList = [tweet for tweet in tweet_dict if tweet['user']['id_str'] in samsaSetFiltered or 
                                                                           tweet['in_reply_to_user_id_str'] in samsaSetFiltered]
                        with open(folderPath + "samsa" + lastDayFolder + ".json", "a") as samsaArchiveFile:
                            simplejson.dump(tweetSamsaList, samsaArchiveFile)
                            samsaArchiveFile.close()
                            
                        if os.path.isfile(filepath):
                            os.remove(filepath)
                            
                            
def clean_json_files(filePath):
    
    '''
    Method used to clean the Json files written by the retrieve_samsa_function, in order to correct any parsing error happening.
    This function concatenates every list present in a Json file into a single list.
    '''

    with open(filePath) as samsaFile:
        samsaText = samsaFile.read()

        samsaTextCleaned = samsaText.replace('][', ',')
        with open(filePath, 'w') as writeFile:
            writeFile.write(samsaTextCleaned)
            
            
def language_filtering(dirpath, file, folder):
    
    '''
    The next filtering step consists of keeping only the tweets posted by accounts whose languages is among the ones we want
    to study (i.e lang in ['en', 'es', 'fr', 'ru', 'uk', 'tr', 'de']). This method will be used to create a new database, by 
    keeping only the tweets posted by users with one of these languages in their profile
    '''
    filePath = os.path.join(dirpath, file)
    langFiltered = ['en', 'es', 'fr', 'ru', 'uk', 'tr', 'de']
    totalCount, langCount = 0, 0
    langTime = time.time()
    
    langList = []
    
    with open(filePath, 'rb') as jsonFile:
        objects = ijson.items(jsonFile, 'item')
        tweets = (o for o in objects)
        for tweet in tweets:
            totalCount += 1
            if tweet['lang'] in langFiltered:
                langCount += 1
                langList.append(tweet)
    
    languageFile = os.path.join("Lang", folder, file[:-5] + "_lang"  + ".json")
    
    with open(languageFile, 'w') as jsonFilteredFile:
        simplejson.dump(langList, jsonFilteredFile)
        jsonFilteredFile.close()
        
    
def remove_duplicate_tweets():
    
    langDir = 'Lang/'
    
    for dirpath, dirnames, filenames in os.walk(langDir):
        if len(filenames) >= 1: # at least one file is nested in the directory
            for file in filenames:
                
                filePath = os.path.join(dirpath, file)
                with open(filePath, 'rb') as jsonFile:
                    objects = ijson.items(jsonFile, 'item')
                    tweets = (o for o in objects)
                    
                    tweetList = [tweet for tweet in tweets]
                    
                    for firstDigit in range(1, 10):
                        for secondDigit in range(0, 10):
                            for thirdDigit in range(0, 10):
                                for fourthDigit in range(0, 10):
                                    for fifthDigit in range(0, 10):
                                        digitList = [tweet for tweet in tweetList if tweet['id_str'][0] == str(firstDigit)
                                                                                 and tweet['id_str'][1] == str(secondDigit)
                                                                                 and tweet['id_str'][2] == str(thirdDigit)
                                                                                 and tweet['id_str'][3] == str(fourthDigit)
                                                                                 and tweet['id_str'][4] == str(fifthDigit)]
                                
                    
                    set_of_jsons = {simplejson.dumps(d, sort_keys=True) for d in tweetList}
                    X = [simplejson.loads(t) for t in set_of_jsons]
    
def language_shift_detection():
    
    '''
    Method used to study if any Samsa candidate has used different languages throughout the year 2018, by building a dataframe
    listing every Samsa candidate and the number of tweets they posted in each language
    '''
    
    langDir = 'Lang/'
    dfColumns = ['samsa_id', 'en', 'es', 'fr', 'ru', 'uk', 'tr', 'de']
    
    langDf = pd.DataFrame(columns=dfColumns)
    tweetCount = 0
    
    for dirpath, dirnames, filenames in os.walk(langDir):
        if len(filenames) >= 1: # at least one file is nested in the directory
            for file in filenames:
                
                filePath = os.path.join(dirpath, file)
                with open(filePath, 'rb') as jsonFile:
                    objects = ijson.items(jsonFile, 'item')
                    tweets = (o for o in objects)
                    print("Starting file: ", filePath)
                    
                    langDict = [{'id': tweet['user']['id_str'], 'lang': tweet['lang']} for tweet in tweets]
                    idListSorted = sorted(list(set([langTweet['id'] for langTweet in langDict])))
                    
                    langDictGrouped = groupby(sorted(langDict, key=itemgetter('id')) , key=itemgetter('id'))

                    langGroup = [[tweet['lang'] for tweet in list(groupList)] for idUser, groupList in langDictGrouped]
                    
                    langCount = [Counter(group) for group in langGroup]
                    
                    langIdDict = [dict(countDict, **{'samsa_id':idUser}) for countDict, idUser in zip(langCount, idListSorted)]
                    
                    langDataframe = pd.DataFrame(columns=dfColumns)
                    langDataframe = langDataframe.append(langIdDict)
                    langDataframe = langDataframe.fillna(0)
                    langDataframe[dfColumns] = langDataframe[dfColumns].applymap(np.int32)
                    
                    with open('samsaLang.csv', 'a') as samsaLangFile:
                        langDataframe.to_csv(samsaLangFile, sep='\t')   
                        
def merge_dataframe():
    
    '''
    This method is used to open the dataframe created with the language_shift_detection function, in order to sum up every row relative to the same ID. The new
    dataframe is then saved in another .csv file
    '''
    
    dfColumns = ['samsa_id', 'en', 'es', 'fr', 'ru', 'uk', 'tr', 'de']
    langDask = dd.read_csv('samsaLang.csv', sep='\t', usecols=dfColumns, dtype={'de': 'object',
       'en': 'object',
       'es': 'object',
       'fr': 'object',
       'ru': 'object',
       'samsa_id': 'object',
       'tr': 'object',
       'uk': 'object'})
    
    print("Read file")
    langDask = langDask.groupby('samsa_id').sum()
    print("Groupby ok")
    
    daskTest = langDask.head().compute()
    print("Test: ", daskTest)
    daskTest.to_csv('langDask.csv')
    
def filter_language_candidates():
    
    '''
    Method called right after the identify_image_modification method, in order to keep from the Samsa candidates only those who have posted tweets in the 
    languages of interest
    '''
    
    langFiltered = ['en', 'es', 'fr', 'ru', 'uk', 'tr', 'de']
    samsaFilteredList = [] # List used to store the Samsa candidates that posted in the languages of interest
    langDir = 'Lang'
    
    for dirpath, dirnames, filenames in os.walk(langDir):
        for file in filenames:
            
            filePath = os.path.join(dirpath, file)
            with open(filePath, 'rb') as jsonFile:
                fileTime = time.time()
                objects = ijson.items(jsonFile, 'item')
                tweets = (o for o in objects)
                print("Starting file: ", filePath)
                
                fileList = [tweet['id_str'] for tweet in tweets]
                fileIDList = [tweet['id_str'] for tweet in tweets if tweet['id_str'] in samsaIDList]
                samsaFilteredList.extend(fileList)
                
                print("Len: ", len(samsaFilteredList), time.time() - fileTime)
    
    return samsaFilteredList
    
    
def training_dataset_creation(samsaIDList):
    
    '''
    This method takes as an input list of IDs of random Samsa candidates and retrieves every tweet published by those users, in order to create a smaller dataset
    for the annotation and training task
    '''
    
    langDir = 'Months Done/'
    samsaTest = samsaIDList[0]
    
    for dirpath, dirnames, filenames in os.walk(langDir):
        for file in filenames:
            
            print("File: ", file, dirpath, dirnames)
            filePath = os.path.join(dirpath, file)
            with open(filePath, 'rb') as jsonFile:
                objects = ijson.items(jsonFile, 'item')
                tweets = (o for o in objects)
                print("Starting file: ", filePath)
                
           
                samsaTweets = [tweet for tweet in tweets if tweet['id_str'] in samsaIDList]
                
                # Writing the dataset in a new file
                if len(samsaTweets) > 0:
                    writeFilePath = os.path.join('Annotation Dataset', dirpath, file)
                    with open(writeFilePath, 'w') as writeFile:
                        simplejson.dump(samsaTweets, writeFile)
                else:
                    print("Empty")
                    
def samsa_candidate_analysis():
    
    '''
    Method called to retrieve any information related to a specific Samsa candidate, in order to classify it
    '''
    
    samsaID = input('Please enter the ID of the Samsa candidate: ')
    samsaTweets = []
    
    print(samsaID, type(samsaID))
    
    # Look for any occurence of that specific ID in the training dataset
    
    for dirpath, dirnames, filenames in os.walk('Annotation Dataset'):
        for file in filenames:
            
            filePath = os.path.join(dirpath, file)
            with open(filePath, 'rb') as jsonFile:
                objects = ijson.items(jsonFile, 'item')
                tweets = (o for o in objects)

                samsaTweets.extend([tweet for tweet in tweets if tweet['id_str'] == samsaID])
                
    dateList = [datetime.strptime(tweet['created_at'], '%a %b %d %H:%M:%S %z %Y') for tweet in samsaTweets]
    
    print("DateList: ", dateList)
    
                
    return samsaTweets
                        
def samsa_network_creation(samsaNetwork):
    
    '''
    Goal: create a network containing two types of nodes
    
        - A node for each of the Samsa user
        - Some nodes representing each of the selected language (i.e Russian, French, English, Turkish, Spanish, Ukrainian, German)
        
    Every time a tweet is processed, a new Samsa node is created if it does not already appear, and an edge between this user and the language in which they tweeted
    is added to the network. Each Samsa node will also have a property related to their profile language, which is set to "shifted" if more than one language is
    detected in the archive
    '''
    
    langDir = 'Lang/Apr 2018'
    
    # Add the language nodes to the network
    
    langNodes = ['en', 'es', 'fr', 'ru', 'uk', 'tr', 'de']
    samsaNetwork.add_nodes_from(langNodes, profileLanguage = 'None')
    
    for dirpath, dirnames, filenames in os.walk(langDir):
        if len(filenames) >= 1:
            for file in filenames:
                
                filePath = os.path.join(dirpath, file)
                print("Starting file: ", filePath)
                
                with open(filePath, 'rb') as jsonFile:
                    objects = ijson.items(jsonFile, 'item')
                    tweets = (o for o in objects)
                    for tweet in tweets:
                        
                        # Process the data relative to the user
                        
                        idUser = tweet['user']['id_str']
                        langUser = tweet['user']['lang']
                        followerUser = tweet['user']['followers_count']
                        if idUser not in samsaNetwork.nodes():
                            samsaNetwork.add_node(idUser)
                            samsaNetwork.nodes[idUser]['profileLanguage'] = langUser
                            samsaNetwork.nodes[idUser]['followersCount'] = followerUser
                            #print("New user: ", samsaNetwork.nodes[idUser]['profileLanguage'], samsaNetwork.nodes[idUser]['followersCount'])
                        else:
                            if langUser is not samsaNetwork.nodes[idUser]['profileLanguage']:
                                samsaNetwork.nodes[idUser]['profileLanguage'] = 'shifted'
                                
                            if followerUser > samsaNetwork.nodes[idUser]['followersCount']:
                                samsaNetwork.nodes[idUser]['followersCount'] = followerUser
                                
                        # Process the data relative to the tweet
                        
                        langTweet = tweet['lang']
                        
                        if langTweet not in samsaNetwork.nodes():
                            langNodes.append(langTweet)
                            samsaNetwork.add_node(langTweet, profileLanguage='None')
                            print("New language: ", langTweet)
                        
                        if samsaNetwork.has_edge(idUser, langTweet):
                            samsaNetwork[idUser][langTweet]['weight'] += 1
                        else:
                            samsaNetwork.add_edge(idUser, langTweet, weight = 1)
                            
                            
            # Split the graph in multiple graphs, relative to the nodes' profileLanguage attribute
            
            samsaGerman = samsaNetwork.copy()
            samsaGerman.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'de' and node not in langNodes])
            nx.write_gexf(samsaGerman, 'samsaGermanapr.gexf', version="1.2draft")
            
            samsaEnglish = samsaNetwork.copy()
            samsaEnglish.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'en' and node not in langNodes])
            nx.write_gexf(samsaEnglish, 'samsaEnglishapr.gexf', version="1.2draft")
            
            samsaFrench = samsaNetwork.copy()
            samsaFrench.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'fr' and node not in langNodes])
            nx.write_gexf(samsaFrench, 'samsaFrenchapr.gexf', version="1.2draft")
            
            samsaRussian = samsaNetwork.copy()
            samsaRussian.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'ru' and node not in langNodes])
            nx.write_gexf(samsaRussian, 'samsaRussianapr.gexf', version="1.2draft")
            
            samsaUkrainian = samsaNetwork.copy()
            samsaUkrainian.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'uk' and node not in langNodes])
            nx.write_gexf(samsaUkrainian, 'samsaUkrainianapr.gexf', version="1.2draft")
            
            samsaTurkish = samsaNetwork.copy()
            samsaTurkish.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'tr' and node not in langNodes])
            nx.write_gexf(samsaTurkish, 'samsaTurkishapr.gexf', version="1.2draft")
            
            samsaSpanish = samsaNetwork.copy()
            samsaSpanish.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'es' and node not in langNodes])
            nx.write_gexf(samsaSpanish, 'samsaSpanishapr.gexf', version="1.2draft")
            
            samsaShifted = samsaNetwork.copy()
            samsaShifted.remove_nodes_from([node for node in samsaNetwork.nodes() if samsaNetwork.nodes[node]['profileLanguage'] != 'shifted' and node not in langNodes])
            nx.write_gexf(samsaShifted, 'samsaShiftedapr.gexf', version="1.2draft")
            
            print("Before: ", len(samsaNetwork.nodes()))
            nodesToRemove = []
            for (node, degree) in samsaNetwork.degree:
                if degree == 1:
                    nodesToRemove.append(node)
                    
            samsaNetwork.remove_nodes_from(nodesToRemove)
            nx.write_gexf(samsaNetwork, 'apr2018.gexf', version="1.2draft")

            
            print("--TOTAL NUMBER OF NODES--")
            print("German: ", len(samsaGerman.nodes()), '\n',
                  "French: ", len(samsaFrench.nodes()), '\n',
                  "Spanish: ", len(samsaSpanish.nodes()), '\n',
                  "Russian: ", len(samsaRussian.nodes()), '\n',
                  "Ukrainian: ", len(samsaUkrainian.nodes()), '\n',
                  "Turkish: ", len(samsaTurkish.nodes()), '\n',
                  "English: ", len(samsaEnglish.nodes()), '\n',
                  "Shifted: ", len(samsaShifted.nodes()), '\n',
                  "Total: ", len(samsaNetwork.nodes()), '\n')
                        
def detect_samsa_candidates(filePath, updatedFilePath):
    
    '''
    Method used to detect Samsa candidates in the archives related to the Yellow Vest Movements and Trump's electoral campaign, by using the following rules:
    
    - %x of screen name changed in terms of edit distance
    - tweet count is decreased by %y
    - follower count is decreased by %z
    '''
    
    samsaList = []
    
    samsaDF = pd.read_csv(filePath, engine='python').drop_duplicates()
    updatedSamsaDF = pd.read_csv(updatedFilePath).drop_duplicates().dropna()
    
    idList = samsaDF.id
    updatedIdList = updatedSamsaDF.id
    digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    idList = [idSamsa for idSamsa in idList if idSamsa.isnumeric()]
    idList = [int(idSamsa) for idSamsa in idList]
    updatedIdList = [idSamsa for idSamsa in updatedIdList if str(idSamsa).isnumeric()]
    updatedIdList.extend(idList)
    
    # 1st step: Analyze any ID with less than three digits
    shortList = [idSamsa for idSamsa in updatedIdList if len(str(idSamsa)) < 4]
    samsaList.extend([{'id': item, 'count': count} for item, count in Counter(shortList).items() if count > 1])
    
    updatedIdList = [idSamsa for idSamsa in updatedIdList if idSamsa not in shortList]
            
    for firstDigit in range(1, 10):
        for secondDigit in range(0, 10):
            for thirdDigit in range(0, 10):
                idDigitList = [samsaID for samsaID in updatedIdList if str(samsaID)[0] == str(firstDigit) and str(samsaID)[1] == 
                               str(secondDigit) and str(samsaID)[2] == str(thirdDigit)]
                samsaList.extend([{'id': item, 'count': count} for item, count in Counter(idDigitList).items() if count > 1])
                
    idSamsaList = [samsa['id'] for samsa in samsaList]
    countSamsaList = [samsa['count'] for samsa in samsaList]
                
    return idSamsaList, countSamsaList

def samsa_candidates_analysis(filePath, updatedFilePath, idSamsaList):
    
    digits = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.']
    
    samsaDF = pd.read_csv(filePath, engine='python', usecols=['id', 'description', 'followers_count', 'screen_name',
                                                              'name', 'favourites_count', 'friends_count',
                                                              'language', 'location', 'created_at', 'listed_count']).drop_duplicates().dropna(subset=['id', 'followers_count', 
                                                                                                                                                      'listed_count', 
                                                                                                                                                      'screen_name'])
    followSamsa = list(samsaDF.followers_count)
    followFiltered = [follow for follow in followSamsa if len(''.join(char for char in follow if char not in digits)) > 0]
    
    samsaDFFiltered = samsaDF[~samsaDF.followers_count.isin(followFiltered)]
    samsaDFFiltered['followers_count'] = samsaDFFiltered['followers_count'].astype(float)
    
    samsaDF['id'] = samsaDF['id'].astype(str)
    
    updatedSamsaDF = pd.read_csv(updatedFilePath, usecols=['id', 'description', 'followers_count', 'screen_name',
                                                              'name', 'favourites_count', 'friends_count',
                                                              'lang', 'location', 'created_at', 'listed_count']).drop_duplicates().dropna(subset=['id', 'followers_count', 
                                                                                                                                                  'listed_count', 
                                                                                                                                                  'screen_name'])
    updatedFollowSamsa = list(updatedSamsaDF.followers_count)
    updatedFollowFiltered = [follow for follow in updatedFollowSamsa if len(''.join(char for char in str(follow) if char not in digits)) > 0]
    
    updatedSamsaDFFiltered = updatedSamsaDF[~updatedSamsaDF.followers_count.isin(updatedFollowFiltered)]
    updatedSamsaDFFiltered['followers_count'] = updatedSamsaDFFiltered['followers_count'].astype(float)
    
    
    updatedSamsaDF['id'] = updatedSamsaDF['id'].astype(str)
    
    mergedSamsaDF = updatedSamsaDF.append(samsaDF, ignore_index=True)
    groupedSamsa = mergedSamsaDF.groupby(mergedSamsaDF['id'])
    
    validTime = time.time()
    validSamsaCandidates = [group for name, group in groupedSamsa if 'nan' not in [str(row.followers_count) for index, row in group.iterrows()]]

    featureTime = time.time()
    samsaFeaturesList = [{'screen_names': [str(row.screen_name) for index, row in group.iterrows()],
                         'followers_count': [row.followers_count for index, row in group.iterrows()],
                         'listed_count': [row.listed_count for index, row in group.iterrows()]} 
                         for name, group in groupedSamsa]
    
    print("Features: ", len(samsaFeaturesList), samsaFeaturesList[0:10], time.time() - featureTime)
    
    return samsaFeaturesList
    
def samsa_features_analysis(samsaFeaturesList, updatedFilePath):
    
    '''
    Method called to analyse the Samsa candidates derived with the samsa_candidates_analysis method, in order to see which accounts are eligible to be tagged as
    Samsa, i.e they changed their identity at least once
    '''
    
    samsaFeatureFiltered = [samsaFeature for samsaFeature in samsaFeaturesList if len(samsaFeature['screen_names']) > 1]
    
    # 1st step: analysis of the edit distance for each candidate relative to their screen names
    
    screenNameList = [[samsaName for samsaName in samsaCandidate['screen_names']] for samsaCandidate in samsaFeatureFiltered]
    
    editDistanceList = [Levenshtein.distance(screenName1, screenName2) for [screenName1, screenName2] in screenNameList]
    
    # 2nd step: analysis of the followers count change for each candidate
    
    followerCountList = [[int(float(follower)) for follower in samsaCandidate['followers_count']] for samsaCandidate in samsaFeatureFiltered]
            
    followerRatioList = [(np.max([followerCount1, followerCount2]) - np.min([followerCount1, followerCount2])) / 
                         np.max([followerCount1, followerCount2]) for [followerCount1, followerCount2] in followerCountList]
    
    samsaFeaturesDict = [{'screenNames': screenName, 'editDistance': editDistance, 'followerRatio': followerRatio}
                        for (screenName, editDistance, followerRatio) in 
                        zip(screenNameList, editDistanceList, followerRatioList) if editDistance > 0]
    
    # Visualisation of the edit distance and the follower ratio lists with histograms
    
    editDistanceListFiltered = [samsaCandidate['editDistance'] for samsaCandidate in samsaFeaturesDict]
    followerRatioListFiltered = [samsaCandidate['followerRatio'] for samsaCandidate in samsaFeaturesDict]
    
    plt.hist(editDistanceListFiltered, bins=30)
    plt.title('Edit Distance')
    
    # To filter the Samsa candidates, we only keep the ones whose edit distance and follower ratio is above the average value
    
    editDistanceThresh = 8.5/5 * np.mean(editDistanceListFiltered)
    followerRatioThresh = 8.5/5 * np.nanmean(followerRatioListFiltered)
    
    samsaCandidatesFeature = [samsaCandidate for samsaCandidate in samsaFeaturesDict 
                              if samsaCandidate['editDistance'] > editDistanceThresh 
                              and samsaCandidate['followerRatio'] > followerRatioThresh]
    
    samsaCandidatesScreenNames = [samsaScreenName for samsaCandidate in 
                                  samsaCandidatesFeature for samsaScreenName in samsaCandidate['screenNames']]
    
    print("Candi: ", len(samsaCandidatesScreenNames), samsaCandidatesScreenNames[0:10])
    
    updatedSamsaDF = pd.read_csv(updatedFilePath,usecols=['id', 'description', 'followers_count', 'screen_name',
                                                              'name', 'favourites_count', 'friends_count',
                                                              'lang', 'location', 'created_at', 
                                                          'listed_count']).drop_duplicates().dropna(subset=['id', 
                                                                                                            'followers_count', 
                                                                                                            'listed_count', 
                                                                                                            'screen_name'])
    
    candidatesDF = updatedSamsaDF[updatedSamsaDF.screen_name.isin(samsaCandidatesScreenNames)]
    idSamsaCandidates = [samsaCandidate.id for index, samsaCandidate in candidatesDF.iterrows()]
    
    with open('idSamsaCandidates.json', 'w') as samsaFile:
        simplejson.dump(idSamsaCandidates, samsaFile)
    
    candidatesDF.to_csv('candidatesDF.csv')
    
    
    
    
                            
                        
                        
                    
                
            
    
    
    
    
                                  
                                
                